pydocstyle<4.0.0
flake8
flake8-quotes
flake8-docstrings>=0.2.7
flake8-import-order>=0.9
pep8-naming
flake8-colors
mypy